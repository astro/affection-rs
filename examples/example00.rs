use affection::*;

struct Example {
}

impl Callbacks for Example {
    fn load_state() -> Self {
        Example {}
    }
    fn handle_event(&mut self, ev: Event) {}
    fn update(&mut self) {}
    fn draw(&self, graphics: &mut Graphics) {}
    fn clean_up(self) {}
}

pub fn main() {
    run::<Example>(Config::default());
}
