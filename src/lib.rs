pub mod graphics;
pub use graphics::{Graphics, Event};

pub struct Config {
    window_title: String,
    window_size: (u32, u32),
}

impl Default for Config {
    fn default() -> Self {
        Config {
            window_title: "Affection".to_owned(),
            window_size: (800, 600),
        }
    }
}

pub trait Callbacks {
    fn load_state() -> Self;
    fn pre_loop(&mut self) {
    }
    fn handle_event(&mut self, ev: Event);
    fn update(&mut self);
    fn draw(&self, graphics: &mut Graphics);
    fn clean_up(self);
}

pub fn run<C: Callbacks>(config: Config) {
    let mut graphics = Graphics::new(&config);

    let mut state = C::load_state();
    state.pre_loop();
    loop {
        state.update();
        for ev in graphics.get_events() {
            state.handle_event(ev);
        }
        state.draw(&mut graphics);
    }
    state.clean_up();
}

#[cfg(test)]
mod tests {
    use super::Config;

    #[test]
    fn it_works() {
        let _config = Config::default();
    }
}
